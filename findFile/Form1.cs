﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;


namespace findFile
{
    public partial class Form1 : Form
    {
        DateTime date = DateTime.Now;
        Timer timer = new Timer();


        public Form1()
        {
            InitializeComponent();

            timer.Interval = 10;
            timer.Tick += new EventHandler(tickTimer);

        }


        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (this.folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                this.textBox1.Text = this.folderBrowserDialog1.SelectedPath;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(textBox1.Text);
                treeView1.Nodes.Clear();
                timer.Start();
                Tree(dir, treeView1.Nodes);
            }
            catch (UnauthorizedAccessException ex)
            {
                timer.Stop();
                MessageBox.Show(ex.Message);
            }
            catch (OutOfMemoryException ex)
            {
                timer.Stop();
                MessageBox.Show(ex.Message);
            }

        }

        private void Tree(DirectoryInfo directoryinfo, TreeNodeCollection addInMe)
        {

            TreeNode curNode = addInMe.Add(directoryinfo.Name);
            int count = 0;

            foreach (DirectoryInfo subdir in directoryinfo.GetDirectories())
            {

                Tree(subdir, curNode.Nodes);
            }

            foreach (FileInfo file in directoryinfo.GetFiles(textBox2.Text))
            {

                count++;
                label5.Text = file.FullName;
                label4.Text = Convert.ToString(count);
                if (File.ReadAllText(file.FullName).Contains(textBox3.Text))
                {
                    curNode.Nodes.Add(file.Name);
                }

            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            treeView1.Nodes.Clear();
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            timer.Stop();
            timeLabel.Text = "00:00:00:00";
        }

        private void tickTimer(object sender, EventArgs e)
        {
            long tick = DateTime.Now.Ticks - date.Ticks;
            DateTime stopWatch = new DateTime();
            stopWatch = stopWatch.AddTicks(tick);
            timeLabel.Text = String.Format("{0:HH:mm:ss:ff}", stopWatch);
        }

        private void button2_Click(object sender, EventArgs e)
        {

            timer.Stop();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}

